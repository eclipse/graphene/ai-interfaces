// ===================================================================================
// Copyright (C) 2019 Fraunhofer Gesellschaft. All rights reserved.
// ===================================================================================
// This Graphene software file is distributed by Fraunhofer Gesellschaft
// under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// This file is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ===============LICENSE_END==========================================================
syntax = "proto3";

message SudokuDesignEvaluationJob {
  // 0 means empty, 1-9 means the respective digit is in that field
  // addressing is as follows: field[column+9*row]
  // this field always has 81 elements
  repeated int32 field = 1;
}

message SudokuDesignEvaluationResult {
  enum SolStatus {
    NO_SOLUTION = 0;
    UNIQUE_SOLUTION = 1;
    MULTIPLE_SOLUTIONS = 2;
  }
  SolStatus status = 1;

  // encoding as SudokuDesignEvaluationJob.field, present only if status = 1, all values are set to != 0
  repeated int32 solution = 2;

  // encoding as SudokuDesignEvaluationJob.field, present only if status = 0
  // a minimal set of values that are sufficient for having no solution are set, all others are 0
  repeated int32 minimal_unsolvable = 3;
}

message Parameters {
  // 0 means no limit (do not use that!)
  int number_of_answers = 1;
  bool return_only_optimal_answers = 2;
}

message SolverJob {
  // ASP Core-2 program
  string program = 1;
  Parameters parameters = 2;
}

message CostElement {
  int level = 1;
  int cost = 2;
}

message Answerset {
  repeated string atoms = 1;
  repeated CostElement costs = 2;
  bool is_known_optimal = 3;
}

message ResultDescription {
  bool success = 1;
  int code = 2;
  repeated string messages = 3;
}

message SolveResultAnswersets {
  ResultDescription description = 1;
  repeated Answerset answers = 2;
}

service SudokuDesignEvaluationComponent {
  rpc evaluateSudokuDesign(SudokuDesignEvaluationJob) returns SolverJob;
  rpc processSolverResult(SolveResultAnswersets) returns(SudokuDesignEvaluationResult);
}